/*
 * FAux.h
 *
 *  Created on: 30 May 2021
 *  Author: Alena Grebneva
 */

//#ifndef INC_FAUX_H_
//#define INC_FAUX_H_

#include "lpc_types.h"
#include <stdio.h>
#include "board.h"
#include "arm_math.h"
#include "Onewire.h"

#ifdef __cplusplus
extern "C" {
#endif

void sendTime(RTC_TIME_T *pTime);
void sendData(uint8_t eArray[], uint8_t dArray[], int n);
void showHour(RTC_TIME_T *pTime);
void showTemperature(int sens, int temp);
void showTemperatureI(int sens, int temp);
void showTemperatureF(float32_t *pTemp, int indx);
//int32_t correctTemp(int sens, int tempInt);
void RTC_Init(void);
void IO_Init(void);
void UART3_Init(void);
//void Timer0_Init(void);
int SensorsInit(int *mState, uint8_t mGrp[], uint8_t mLbl[], uint8_t mPort[], uint8_t mPin[], int n);
void ReadTemp(float32_t *pTempRAW, int mState[], uint8_t mPort[], uint8_t mPin[], int n);
void SplitTemp(float32_t *pTemp, uint8_t *pE, uint8_t *pD, int n);
//#endif /* INC_FAUX_H_ */
