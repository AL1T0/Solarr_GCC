# Summary

Date : 2021-09-06 02:22:02

Directory c:\TPF_ED3\Solarr_GCC_V1\example

Total : 7 files,  1031 codes, 686 comments, 270 blanks, all 1987 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C | 5 | 991 | 635 | 244 | 1,870 |
| C++ | 2 | 40 | 51 | 26 | 117 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 7 | 1,031 | 686 | 270 | 1,987 |
| inc | 2 | 40 | 51 | 26 | 117 |
| src | 5 | 991 | 635 | 244 | 1,870 |

[details](details.md)