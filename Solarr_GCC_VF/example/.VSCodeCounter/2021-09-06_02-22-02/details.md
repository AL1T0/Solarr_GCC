# Details

Date : 2021-09-06 02:22:02

Directory c:\TPF_ED3\Solarr_GCC_V1\example

Total : 7 files,  1031 codes, 686 comments, 270 blanks, all 1987 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [inc/FAux.h](/inc/FAux.h) | C++ | 25 | 6 | 5 | 36 |
| [inc/Onewire.h](/inc/Onewire.h) | C++ | 15 | 45 | 21 | 81 |
| [src/FAux.c](/src/FAux.c) | C | 197 | 152 | 55 | 404 |
| [src/Onewire.c](/src/Onewire.c) | C | 199 | 58 | 60 | 317 |
| [src/Solarr.c](/src/Solarr.c) | C | 262 | 214 | 80 | 556 |
| [src/cr_startup_lpc43xx.c](/src/cr_startup_lpc43xx.c) | C | 302 | 166 | 35 | 503 |
| [src/sysinit.c](/src/sysinit.c) | C | 31 | 45 | 14 | 90 |

[summary](results.md)