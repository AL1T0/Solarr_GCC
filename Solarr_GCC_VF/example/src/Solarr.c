/* Solarr GCC
 * Source code for the automation of the measurement of thermal coefficents
 * in a solar panel
 *
 * @note
 * Copyright(C) 2021 Alena Grebneva
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "../inc/Onewire.h"
#include "../inc/FAux.h"
#include "board.h"
#include <math.h>
#include "arm_math.h"
#include <stdlib.h>
//#if defined(SEMIHOSTING)
#include <stdio.h>
//#endif

/*****************************************************************************
 * Definitions
 ****************************************************************************/
#define nTemp 6

/* Outputs Auxiliares */
// Salida de repetición de tecla 1
#define Aux1On		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 5, 14)
#define Aux1Off		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 5, 14)

// Salida de repetición de tecla 2
#define Aux2On		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 5, 12)
#define Aux2Off		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 5, 12)

// Salida de repetición de tecla 3
#define Aux3On		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 2, 6)
#define Aux3Off		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 2, 6)

// Salida de repetición de tecla 4
#define Aux4On		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 2, 5)
#define Aux4Off		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 2, 5)

// Salida de indicación de termalización
#define TermOn		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 2, 4)
#define TermOff		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 2, 4)

// Salida de indicación de alarma
#define AlarmOn		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 3, 3)
#define AlarmOff	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 3, 3)

// On/Off LEDs
#define LedROn		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 5, 0);
#define LedROff		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 5, 0);
#define LedGOn		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 5, 1);
#define LedGOff		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 5, 1);
#define LedBOn		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 5, 2);
#define LedBOff		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 5, 2);
#define Led1On		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 0, 14);
#define Led1Off		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 0, 14);
#define Led2On		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 1, 11);
#define Led2Off		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 1, 11);
#define Led3On		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 1, 12);
#define Led3Off		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, 1, 12);

// Salida de inicialización
#define IniCIAA		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, 3, 12);

// Entradas auxiliares
#define fSendData	Chip_GPIO_GetPinState(LPC_GPIO_PORT, 5, 8)
#define fIniESP32	Chip_GPIO_GetPinState(LPC_GPIO_PORT, 5, 9)

#define MATCH(n)		((uint8_t) n)

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static volatile int idx = 6;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
// Orden de los sensores dentro de los vectores: S1...S5 + Samb
float32_t tempRAW[nTemp];  	// Vector con las temperaturas leídas
float32_t tempConv[nTemp];	// Vector con las temperaturas ajustadas
float32_t multOutput[nTemp];// Salida intermedia de la multiplicación
float32_t addOutput[nTemp]; // Salida intermedia de la suma
float32_t tempStat[5]; 		// Estadísticas de la medición de temperaturas entre S1...S5 (promedio, desvest, var, min, max)
float32_t tempMinLast;		// Temperatura mínima (incializo valor grande para no hacer lógica adicional en el 1er read)
float32_t tempMaxLast;  	// Temperatura máxima
float32_t tRef;  			// Temperatura de referencia para la termalización

int sensState[nTemp];		// Flag de estado de conexión de los sensores

// Información de los pines a los que estan conectados los sensores
// La conexión es en el orden: S1...S5 + Samb
uint8_t sensGrp[nTemp] = {0,0,7,1,1,1};		// Grupo de pines (X en PX_Y del pinout EDU-CIAA)
uint8_t sensLbl[nTemp] = {0,1,7,16,17,18};	// Etiqueta del pin (Y en PX_Y del pinout EDU-CIAA)
uint8_t sensPort[nTemp]= {0,0,3,0,0,0};		// Puerto GPIO
uint8_t sensPin[nTemp] = {0,1,15,3,12,13};	// Pin del puerto GPIO

uint8_t tempConvE[nTemp]; //Vector con la parte entera de los valores de temperatura
uint8_t tempConvD[nTemp]; //Vector con la parte decimal de los valores de temperatura

uint8_t tempStatE[5]; //Vector con la parte entera de las estadísticas
uint8_t tempStatD[5]; //Vector con la parte decimal de las estadísticas

//Multiplicador para la correción de temperatura. Para el sensor de repuesto reemplazar con 6342
float32_t sensMult[nTemp] = {6369.0,6427.0,6308.0,6304.0,6347.0,6392.0};

//Offset para la correción de temperatura. Para el sensor de repuesto reemplazar con 124800
float32_t sensOffset[nTemp] = {81408.0,17411.0,121650.0,124084.0,97984.0,111053.0};

//División por 1000 para que el resultado quede con dos decimales
float32_t sensDiv[nTemp] = {0.00001,0.00001,0.00001,0.00001,0.00001,0.00001};

arm_status status;	// Estados de los cálculos
uint32_t indx;		// Variable dummy

// Flag de interrrupción RTC
bool fSec = false;		// Secuncia de envío de datos por la UART
// Estado de teclas y entradas auxiliares
bool Tecla1, Tecla2, Tecla3, Tecla4;
int cmdESP32;
int sensStatus;		// Estado de los sensores para enviar al ESP-32

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*---------------------------------------------------------------------------
 * @brief	RTC interrupt handler
 * @return	Nothing
 ---------------------------------------------------------------------------*/
void RTC_IRQHandler(void)
{
	uint32_t sec;
	//uint32_t min;
	//uint32_t hr;

	/* Toggle heart beat LED for each second field change interrupt */
	if (Chip_RTC_GetIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE)) {
		/* Clear pending interrupt */
		Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	}

	/* display timestamp every 1 second in the background */
	sec = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_SECOND);
	if (!(sec % 1)) {
		fSec = true;	/* set flag for background */
	}
}

/*---------------------------------------------------------------------------
 * @brief	UART3 interrupt handler
 * @return	Nothing
 ---------------------------------------------------------------------------*/
void UART3_IRQHandler(void)
{
	if (Chip_UART_ReadIntIDReg(LPC_USART3) & (1<<2))
	{
		cmdESP32 = Chip_UART_ReadByte(LPC_USART3);
		//DEBUGOUT("Dato UART: %d\n",cmdESP32);
	}
}

/*---------------------------------------------------------------------------
 * @brief	GPIO Button 1 interrupt handler
 * @return	Nothing
 ---------------------------------------------------------------------------*/
void GPIO0_IRQHandler(void){
	//Interrupción por acción de la Tecla 1
	bool T1_Rise = (LPC_GPIO_PIN_INT->RISE)&(PININTCH0); // Pulsador presionado
	bool T1_Fall = (LPC_GPIO_PIN_INT->FALL)&(PININTCH0); // Pulsador soltado

	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH0);

	if (T1_Rise) { // Verifico si la interrupción fue por flanco ascendente
		Aux1Off;
		Tecla1 = false;
	}
	else
	{
		if (T1_Fall) { 	// Verifico si la interrupción fue por flanco descendente
			Aux1On;
			Tecla1 = true;
		}
	}
}

/*---------------------------------------------------------------------------
 * @brief	GPIO Button 2 interrupt handler
 * @return	Nothing
 ---------------------------------------------------------------------------*/
void GPIO1_IRQHandler(void){
	//Interrupción por acción de la Tecla 2
	bool T2_Rise = (LPC_GPIO_PIN_INT->RISE)&(PININTCH1); // Pulsador presionado
	bool T2_Fall = (LPC_GPIO_PIN_INT->FALL)&(PININTCH1); // Pulsador soltado

	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH1);

	if (T2_Rise) { // Verifico si la interrupción fue por flanco ascendente
		Aux2Off;
		Tecla2 = true;
	}
	else
	{
		if (T2_Fall) { 	// Verifico si la interrupción fue por flanco descendente
			Aux2On;
			Tecla2 = true;
		}
	}
}

/*---------------------------------------------------------------------------
 * @brief	GPIO Button 3 interrupt handler
 * @return	Nothing
 ---------------------------------------------------------------------------*/
void GPIO2_IRQHandler(void){
	//Interrupción por acción de la Tecla 3
	bool T3_Rise = (LPC_GPIO_PIN_INT->RISE)&(PININTCH2); // Pulsador presionado
	bool T3_Fall = (LPC_GPIO_PIN_INT->FALL)&(PININTCH2); // Pulsador soltado

	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH2);

	if (T3_Rise) { // Verifico si la interrupción fue por flanco ascendente
		Aux3Off;
		Tecla3 = false;
	}
	else
	{
		if (T3_Fall) { 	// Verifico si la interrupción fue por flanco descendente

			Aux3On;
			Tecla3 = true;
		}
	}
}

/*---------------------------------------------------------------------------
 * @brief	GPIO Button 4 interrupt handler
 * @return	Nothing
 ---------------------------------------------------------------------------*/
void GPIO3_IRQHandler(void){
	//Interrupción por acción de la Tecla 4
	bool T4_Rise = (LPC_GPIO_PIN_INT->RISE)&(PININTCH3); // Pulsador presionado
	bool T4_Fall = (LPC_GPIO_PIN_INT->FALL)&(PININTCH3); // Pulsador soltado

	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH3);

	if (T4_Rise) { // Verifico si la interrupción fue por flanco ascendente
		Aux4Off;
		Tecla4 = false;
	} else {
		if (T4_Fall) { 	// Verifico si la interrupción fue por flanco descendente
			Aux4On;
			Tecla4 = true;
		}
	}
}

/*---------------------------------------------------------------------------
 * @brief	Secuencia de lectura y almacenamiento de los datos de sensores
 * @return	Nothing
 ---------------------------------------------------------------------------*/
void leerMediciones(void) {
	/* Leer los sensores DS18B20 */
	ReadTemp(tempRAW, sensState, sensPort, sensPin, nTemp);

	/* Simular valores de temperatura para pruebas */
	//tempRAW[0]=31.1;
	//tempRAW[1]=32.2;
	//tempRAW[2]=33.3;
	//tempRAW[3]=34.4;
	//tempRAW[4]=35.5;
	//tempRAW[5]=24.4;

	/* Conversión de la temperatura usando las funciones CMSIS DSP
	   Formato: T_°C = ((T_raw * T_mult) + T_offset) / 100;*/
	arm_mult_f32(tempRAW, sensMult, multOutput, nTemp);
	arm_add_f32(multOutput, sensOffset, addOutput, nTemp);
	arm_mult_f32(addOutput, sensDiv, tempConv, nTemp);

	/* Cálculo de las estadísticas de la medición */
	//Temperatura promedio Sensores 1 al 5
	arm_mean_f32(tempConv, nTemp-1, &tempStat[0]);
	//Desviacón estándar
	arm_std_f32(tempConv, nTemp-1, &tempStat[1]);
	//Varianza
	arm_var_f32(tempConv, nTemp-1, &tempStat[2]);
	//Mínimo
	arm_min_f32(tempConv, nTemp-1, &tempMinLast, &indx);
	if (tempMinLast < tempStat[3]) { //Mínimo medido
		tempStat[3] = tempMinLast;
	}
	//Máximo
	arm_max_f32(tempConv, nTemp-1, &tempMaxLast, &indx);
	if (tempMaxLast > tempStat[4]) { //Máximo medido
		tempStat[4] = tempMaxLast;
	}

	/* Partir parte entera y parte decimal para enviar por la UART */
	SplitTemp(tempConv, tempConvE, tempConvD, nTemp);
	//DEBUGOUT("TA: %d.%d\n",tempConvE[5],tempConvD[5]);
	SplitTemp(tempStat, tempStatE, tempStatD, 5);
	//DEBUGOUT("Prom: %d.%d\n",tempStatE[0],tempStatD[0]);
}

/*---------------------------------------------------------------------------
 * @brief	Main entry point
 * @return	Nothing
 ---------------------------------------------------------------------------*/
int main(void)
{
	tempStat[3] = 100.0; 	// Hardcodear T mínima en 100 para que en la primera medición quede con la T mínima medida
	RTC_TIME_T TimeStamp;
	sensStatus = 0;
	bool fIniEnsayo = false, fAlarma = false, fSendTS = false, fSendM = false, fSendS = false;//, fIniCIAA = false;
	//bool stopWeb;
	bool iniEnsUART;
	bool stopEnsUART;
	//bool iniRTC = 0; // flags de inicialización
	int tRefE = 0, tRefD = 0; // Parte entera y decimal de la temperatura de referencia.

	SystemCoreClockUpdate();
	IO_Init();
	RTC_Init();
	UART3_Init();

	IniCIAA;

	// Espera que inicie el ESP32
	while (fIniESP32 == false) {}

	// Envíar estados de los sensores al ESP32 e indicar que la CIAA inició
	sensStatus = SensorsInit(sensState, sensGrp, sensLbl, sensPort, sensPin, nTemp);
	//DEBUGOUT("Estado Sensores: %d\n",sensStatus);
	Chip_UART_SendByte(LPC_USART3,sensStatus);

	//Habilitar interrupción UART3 para recibir comandos del ESP32
	Chip_UART_IntEnable(LPC_USART3, (1 << 0));

	/* Loop para siempre */
	while (1) {

		Chip_RTC_GetFullTime(LPC_RTC, &TimeStamp);
		//showHour(&TimeStamp);

		// Ante recepción de datos por UART, leer el dato del buffer para ejecutar comandos
		switch (cmdESP32)
		{
			case 241:		// Inicio del ensayo
				if (fSendData == true){
					Chip_UART_SendByte(LPC_USART3, 241); // Confirma comando
					iniEnsUART = true;
					stopEnsUART = false;
					//Chip_UART_SendByte(LPC_USART3,241);
					cmdESP32 = 0;
				}
			break;

			case 242:		// Problema de inicialización de algún sensor, se debe repetir la rutina
				sensStatus = 0;
				sensStatus = SensorsInit(sensState, sensGrp, sensLbl, sensPort, sensPin, nTemp);
				Chip_UART_SendByte(LPC_USART3,sensStatus);
				//DEBUGOUT("Estado Sensores: %d\n",sensStatus);
				cmdESP32 = 0;
			break;

			case 243:		// Envía la estampa del tiempo
				if (fSendData == true){
					sendTime(&TimeStamp);
					cmdESP32 = 0;
					fSendTS = false;
				} else {
					if (!fSendTS)
					{
						Chip_UART_SendByte(LPC_USART3, 243);
						fSendTS = true;
					}
				}
			break;

			case 244:		// Envía mediciones de los sensores
				if (fSendData == true){
					sendData(tempConvE, tempConvD, nTemp);
					cmdESP32 = 0;
					fSendM = false;
				} else {
					if (!fSendM)
					{
						Chip_UART_SendByte(LPC_USART3, 244);
						fSendM = true;
					}
				}
			break;

			case 245:		// Envía estadísticas
				if (fSendData == true){
					sendData(tempStatE, tempStatD, 5);
					cmdESP32 = 0;
					fSendS = false;
				} else {
					if (!fSendS)
					{
						Chip_UART_SendByte(LPC_USART3, 245);
						fSendS = true;
					}
				}
			break;

			case 246:		// Recepción de la temperatura de referencia
				tRefE = Chip_UART_ReadByte(LPC_USART3);
				tRefD = Chip_UART_ReadByte(LPC_USART3);
				tRef = (float32_t) (tRefE + (tRefD / 100.0));
				cmdESP32 = 0;
			break;

			case 247:		// Detengo el ensayo
				iniEnsUART = false;
				stopEnsUART = true;
				//Chip_UART_SendByte(LPC_USART3,247);
				cmdESP32 = 0;
			break;

			default:
				//indx = Chip_UART_ReadByte(LPC_USART3);
				cmdESP32 = 0;
			break;
		}

		// Inicio de ensayo con la tecla 1 de la EDUCIAA o a través del web server
		if (iniEnsUART && !fIniEnsayo) {  // Agregar condiciones adicionales de inicio de ensayo
			//DEBUGOUT("Inicio Ensayo \n");
			fIniEnsayo = true;
			iniEnsUART = false;
			LedGOn;
		}

		// Detengo el ensayo con la tecla 2 de la EDUCIAA o a través del web server, o si hay alarma,
		// siempre que no esté en modo de navegación
		if (stopEnsUART && fIniEnsayo) {
			//DEBUGOUT("Detener Ensayo \n");
			fIniEnsayo = false;

			TermOff; // Apagar salida de termalización
			LedGOff; // Apagar led indicador de ensayo en progreso
			Led3Off; // Apagar led indicador de termalización
		}

		// Limpio la alarma con la tecla 3
		if (Tecla3 && fAlarma) {
			//DEBUGOUT("Alarma \n");
			fAlarma = false;

			AlarmOff; 	// Apagar salida de alarma
			Led2Off; 	// Apagar led indicador de alarma
		}

		/* Si está iniciado el ensayo, cada 1s tomar lectura de las mediciones */
		if (fSec && fIniEnsayo) {
			fSec = 0;

			// Inhabilitar interrupción del RTC para que no afecte las pausas de señalización
			NVIC_DisableIRQ((IRQn_Type) RTC_IRQn);

			// actualizar mediciones de temperatiura
			leerMediciones();
			//DEBUGOUT("Leer mediciones \n");

			// Habilitar interrupción del RTC nuevamente
			NVIC_EnableIRQ((IRQn_Type) RTC_IRQn);

			// Cuando llega a la termalización enciende enciende la salida auxiliar para
			// indicarle al ESP-32 que debe empezar a solicitar los datos
			if (tempStat[0] > tRef) {
				TermOn; // Habilita salida de indicación de termalización al ESP-32
				Led3On; // Encender led 3 (verde) para indicar termalización
			}

			// Si la temperatura se va de rango enciende una alarma
			if (tempStat[0] > 55.0) {
				fAlarma = true;	// Enciende flag interno de alarma

				AlarmOn; 	// Encender salida de alarma
				Led2On;		// Encender el led indicador de alarma
			}

		}

	}

}

// Tiki-Tiki
// ============================== eof ==============================
