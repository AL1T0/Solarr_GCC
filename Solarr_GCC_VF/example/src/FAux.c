/* Copyright 2021, Alena Grebneva
 * 
 * Auxiliary functions for Solarr GCC firmware
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Brief for this file.
 **
 **/

/** \addtogroup groupName Group Name
 ** @{ */

/*==================[inclusions]=============================================*/
#include "../inc/FAux.h"
#include "board.h"
#include "chip.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/**
 * @brief	Envía la fecha y hora actual por la UART3
 * @return	Nothing
 */
void sendTime(RTC_TIME_T *pTime)
{
	 /*DEBUGOUT("Time: %.2d:%.2d:%.2d %.2d/%.2d/%.4d\r\n",
			pTime->time[RTC_TIMETYPE_HOUR],
			pTime->time[RTC_TIMETYPE_MINUTE],
			pTime->time[RTC_TIMETYPE_SECOND],
			pTime->time[RTC_TIMETYPE_DAYOFMONTH],
			pTime->time[RTC_TIMETYPE_MONTH],
			pTime->time[RTC_TIMETYPE_YEAR]);*/

	Chip_UART_SendByte(LPC_USART3, (int) pTime->time[RTC_TIMETYPE_DAYOFMONTH]);
	Chip_UART_SendByte(LPC_USART3, (int) pTime->time[RTC_TIMETYPE_MONTH]);
	Chip_UART_SendByte(LPC_USART3, (int) pTime->time[RTC_TIMETYPE_YEAR] - 2000);
	Chip_UART_SendByte(LPC_USART3, (int) pTime->time[RTC_TIMETYPE_HOUR]);
	Chip_UART_SendByte(LPC_USART3, (int) pTime->time[RTC_TIMETYPE_MINUTE]);
	Chip_UART_SendByte(LPC_USART3, (int) pTime->time[RTC_TIMETYPE_SECOND]);
}

/**
 * @brief	Envía las mediciones por la UART3
 * @return	Nothing
 */
void sendData(uint8_t eArray[], uint8_t dArray[], int n)
{
	int s;
	for (s = 0; s < n; s++) {
		//if (! Chip_UART_CheckBusy(LPC_USART3)){
		Chip_UART_SendByte(LPC_USART3,eArray[s]);
		Chip_UART_SendByte(LPC_USART3,dArray[s]);
		//DEBUGOUT("%.2d,%.2d\n",eArray[s],dArray[s]);
		//}
	}
}

/**
 * @brief	Debug, muestra la hora actual
 * @return	Nothing
 */
void showHour(RTC_TIME_T *pTime)
{
	DEBUGOUT("\n%.2d:%.2d:%.2d", pTime->time[RTC_TIMETYPE_HOUR],
			 pTime->time[RTC_TIMETYPE_MINUTE],
			 pTime->time[RTC_TIMETYPE_SECOND]);
}
/**
 * @brief	Debug, muestra lectura de temperatura obtenida
 * @return	Nothing
 */
void showTemperature(int sens, int temp)
{
	char str[20];

	sprintf(str,"\tT%d:\t %d,%04d", sens, temp >> 4, (temp & 0xF) * 625);
	DEBUGSTR(str);
}
/**
 * @brief	Debug, muestra lectura de temperatura corregida en formato entero
 * @return	Nothing
 */
void showTemperatureI(int sens, int temp)
{
	char str[20];
	sprintf(str,"\tT%d:\t %d", sens, temp);
	DEBUGSTR(str);
}
/**
 * @brief	Debug, muestra lectura de temperatura corregida en formato entero
 * @return	Nothing
 */
void showTemperatureF(float32_t *pTemp, int indx)
{
	char str[20];
	sprintf(str,"\t %d", (int) pTemp[indx]);
	DEBUGSTR(str);
}

/**
 * @brief	Aplica el factor de corrección para cada sensor
 * @return	Temperatura en formato entero x1000
 */
/*int32_t correctTemp(int nS, int iTemp)
{
	int32_t temp;
	switch(nS){
	case 0:
		temp = ((iTemp * 6392) + 111053)/100; // Sensor de Temperatura ambiente
		break;
	case 1:
		temp = ((iTemp * 6369) + 81408)/100; // Sensor S1
		break;
	case 2:
		temp = ((iTemp * 6427) + 17411)/100; // Sensor S2
		break;
	case 3:
		temp = ((iTemp * 6308) + 121650)/100; // Sensor S3
		break;
	case 4:
		temp = ((iTemp * 6304) + 124084)/100; // Sensor S4
		break;
	case 5:
		temp = ((iTemp * 6347) + 97984)/100; // Sensor S5
		break;
	// Sensor de repuesto: ((tempInt * 6342) + 124800)/100;
	}

	return temp;
}*/

/**
 * @brief	Inicializa RTC
 * @return	Nothing
 */
void RTC_Init(void)
{
	Chip_RTC_Init(LPC_RTC);
	/*RTC_TIME_T FullTime;
	// Set  time for RTC Manually
    FullTime.time[RTC_TIMETYPE_SECOND]  = 0;
	FullTime.time[RTC_TIMETYPE_MINUTE]  = 30;
	FullTime.time[RTC_TIMETYPE_HOUR]    = 13;
	FullTime.time[RTC_TIMETYPE_DAYOFMONTH]  = 28;
	FullTime.time[RTC_TIMETYPE_DAYOFWEEK]   = 7;
	FullTime.time[RTC_TIMETYPE_DAYOFYEAR]   = 332;
	FullTime.time[RTC_TIMETYPE_MONTH]   = 11;
	FullTime.time[RTC_TIMETYPE_YEAR]    = 2021;

	Chip_RTC_SetFullTime(LPC_RTC, &FullTime);*/

	/* Set the RTC to generate an interrupt on each second */
	Chip_RTC_CntIncrIntConfig(LPC_RTC, RTC_AMR_CIIR_IMSEC, ENABLE);

	/* Enable matching for alarm for second, minute, hour fields only */
	Chip_RTC_AlarmIntConfig(LPC_RTC, RTC_AMR_CIIR_IMSEC | RTC_AMR_CIIR_IMMIN | RTC_AMR_CIIR_IMHOUR, ENABLE);

	/* Clear interrupt pending */
	Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE | RTC_INT_ALARM);

	/* Enable RTC interrupt in NVIC */
	NVIC_EnableIRQ((IRQn_Type) RTC_IRQn);

	/* Enable RTC (starts increase the tick counter and second counter register) */
	Chip_RTC_Enable(LPC_RTC, ENABLE);
}

/**
 * @brief	Inicializacion de los perifericos especificos para la aplicacion
 * @return	Nothing
 */
void IO_Init(void)
{
	/* DEBUG UART */
	// DEBUGINIT();

	// Habilitar bit TRCENA para usar el DWT como generador de delay
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

	/* Inhabilitar interrupciones de GPIO temporalmente para la configuracion */
	NVIC_DisableIRQ(PIN_INT0_IRQn);
	NVIC_DisableIRQ(PIN_INT1_IRQn);
	NVIC_DisableIRQ(PIN_INT2_IRQn);
	NVIC_DisableIRQ(PIN_INT3_IRQn);
	//NVIC_DisableIRQ(PIN_INT4_IRQn);
	//NVIC_DisableIRQ(PIN_INT5_IRQn);
	//NVIC_DisableIRQ(PIN_INT6_IRQn);

	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT3_IRQn);
	//NVIC_ClearPendingIRQ(PIN_INT4_IRQn);
	//NVIC_ClearPendingIRQ(PIN_INT5_IRQn);
	//NVIC_ClearPendingIRQ(PIN_INT6_IRQn);

	/* Inicializar GPIO */
	Chip_GPIO_Init(LPC_GPIO_PORT);

	/* Inicializar LEDs EDU-CIAA-NXP */
	Chip_SCU_PinMux(2,0,MD_PUP|MD_EZI,FUNC4);	/* GPIO5[0], LED0R */
	Chip_SCU_PinMux(2,1,MD_PUP|MD_EZI,FUNC4);	/* GPIO5[1], LED0G */
	Chip_SCU_PinMux(2,2,MD_PUP|MD_EZI,FUNC4);	/* GPIO5[2], LED0B */
	Chip_SCU_PinMux(2,10,MD_PUP|MD_EZI,FUNC0);	/* GPIO0[14], LED1 */
	Chip_SCU_PinMux(2,11,MD_PUP|MD_EZI,FUNC0);	/* GPIO1[11], LED2 */
	Chip_SCU_PinMux(2,12,MD_PUP|MD_EZI,FUNC0);	/* GPIO1[12], LED3 */

    Chip_GPIO_SetDir(LPC_GPIO_PORT, 5,(1<<0)|(1<<1)|(1<<2),1);
    Chip_GPIO_SetDir(LPC_GPIO_PORT, 0,(1<<14),1);
    Chip_GPIO_SetDir(LPC_GPIO_PORT, 1,(1<<11)|(1<<12),1);

    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 5,(1<<0)|(1<<1)|(1<<2));
    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 0,(1<<14));
    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 1,(1<<11)|(1<<12));

	/* Inicializar Botones */
	Board_Buttons_Init();

	/* Inicializar Salidas de replica de Botones */
    Chip_SCU_PinMux(4, 10, MD_PLN|MD_EZI, FUNC4); // LCD4   - P4_10 - GPIO5[14], Salida para replicar Tecla 1
    Chip_SCU_PinMux(4, 8, MD_PLN|MD_EZI, FUNC4);  // LCD_RS - P4_8  - GPIO5[12], Salida para replicar Tecla 2
    Chip_SCU_PinMux(4, 6, MD_PLN|MD_EZI, FUNC0);  // LCD3   - P4_6  - GPIO2[6],  Salida para replicar Tecla 3
    Chip_SCU_PinMux(4, 5, MD_PLN|MD_EZI, FUNC0);  // LCD2   - P4_5  - GPIO2[5],  Salida para replicar Tecla 4

    Chip_GPIO_SetDir(LPC_GPIO_PORT, 5,(1<<14)|(1<<12),1);
    Chip_GPIO_SetDir(LPC_GPIO_PORT, 2,(1<<6)|(1<<5),1);

    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 5,(1<<14)|(1<<12));
    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 2,(1<<6)|(1<<5));

    /* Iniciar Salida de inicio de la CIAA */
    Chip_SCU_PinMux(7, 4, MD_PLN|MD_EZI, FUNC0);  // TCOL1  - P7_4  - GPIO3[12],  Salida indicar que la CIAA inició

    Chip_GPIO_SetDir(LPC_GPIO_PORT, 3,(1<<12),1);
    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 3,(1<<12));

    /* Inicializa Salidas auxiliares EDU-CIAA -> ESP-32 */
    Chip_SCU_PinMux(4, 4, MD_PLN|MD_EZI, FUNC0);  // LCD1  - P4_4 - GPIO2[4], Salida Auxiliar 1 (termalización)
    Chip_SCU_PinMux(6, 4, MD_PLN|MD_EZI, FUNC0);  // GPIO1 - P6_4 - GPIO3[3], Salida Auxiliar 2 (alarma)

    Chip_GPIO_SetDir(LPC_GPIO_PORT, 2,(1<<4),1);
    Chip_GPIO_SetDir(LPC_GPIO_PORT, 3,(1<<3),1);

    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 2,(1<<4));
    Chip_GPIO_ClearValue(LPC_GPIO_PORT, 3,(1<<3));

    /* Inicializa Entradas auxiliares ESP-32 -> EDU-CIAA */
    Chip_SCU_PinMux(3, 1, MD_PUP|MD_EZI, FUNC4);  // CAN_RD - P3_1 - GPIO5[8], Entrada Auxiliar 1
    Chip_SCU_PinMux(3, 2, MD_PUP|MD_EZI, FUNC4);  // CAN_TD - P3_2 - GPIO5[9], Entrada Auxiliar 2
    //Chip_SCU_PinMux(7, 4, MD_PUP|MD_EZI, FUNC0);  // T_COL1 - P7_4 - GPIO3[12], Entrada Auxiliar 3

    Chip_GPIO_SetDir(LPC_GPIO_PORT, 5,(1<<8)|(1<<9),0);
    //Chip_GPIO_SetDir(LPC_GPIO_PORT, 3,(1<<12),0);

    /* Configurar interrupciones de teclas y entradas auxiliares */
    Chip_SCU_GPIOIntPinSel(0,0,4);	// Interrupción TEC_1 - PININTCH0
	Chip_SCU_GPIOIntPinSel(1,0,8);	// Interrupción TEC_2 - PININTCH1
	Chip_SCU_GPIOIntPinSel(2,0,9);	// Interrupción TEC_3 - PININTCH2
	Chip_SCU_GPIOIntPinSel(3,1,9);	// Interrupción TEC_4 - PININTCH3
	//Chip_SCU_GPIOIntPinSel(4,5,8);	// Interrupción Entrada Auxiliar 1 - PININTCH4
	//Chip_SCU_GPIOIntPinSel(5,5,9);	// Interrupción Entrada Auxiliar 2 - PININTCH5
	//Chip_SCU_GPIOIntPinSel(6,3,12);	// Interrupción Entrada Auxiliar 3 - PININTCH6

	//Configuro interrupcion por flanco
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH0 | PININTCH1 | PININTCH2 | PININTCH3 );
												 // PININTCH4 | PININTCH5 | PININTCH6 );

	//Habilito interrupcion por flanco/nivel bajo
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT,   PININTCH0 | PININTCH1 | PININTCH2 | PININTCH3 );
			 	 	 	 	 	 	 	 	 	 // PININTCH4| PININTCH5 | PININTCH6 );

	//Habilito interrupcion por flanco/nivel alto
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT,  PININTCH0 | PININTCH1 | PININTCH2 | PININTCH3 );
												 //PININTCH4 | PININTCH5 | PININTCH6 );

	/* Habilitar interrupciones de Entradas digitales */
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
	NVIC_EnableIRQ(PIN_INT3_IRQn);
	//NVIC_EnableIRQ(PIN_INT4_IRQn);
	//NVIC_EnableIRQ(PIN_INT5_IRQn);
	//NVIC_EnableIRQ(PIN_INT6_IRQn);
}

/**
 * @brief	Inicializa timer 0 con interrupcion
 * @return	Nothing
 */

/*void Timer0_Init(void)
{
	// Uso el Timer 0 con el MATCH0 y que interrumpa 10 veces por segundo
	Chip_TIMER_Init(LPC_TIMER0); 	//energizamos el timer
	Chip_TIMER_Reset(LPC_TIMER0); 	//reseteamos la cuenta

	Chip_TIMER_SetMatch(LPC_TIMER0,MATCH(0),SystemCoreClock/8); 	//establezco timer a utilizar, match y frecuencia
	Chip_TIMER_MatchEnableInt(LPC_TIMER0,MATCH(0)); 				//Habilitar Interrupcion
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0,MATCH(0)); 			//Habilitamos el Reseteo. Llega a 50ms y resetea.
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER0,MATCH(0));				//Deshabilitamos la parada en match.

	//Chip_TIMER_Enable(LPC_TIMER0);	   // Habilito el Timer 0
	NVIC_ClearPendingIRQ(TIMER0_IRQn); // Limpio interrupciones remanentes
	NVIC_EnableIRQ(TIMER0_IRQn);
	//Chip_TIMER_Enable(LPC_TIMER0);
}*/


/**
 * @brief	Inicializacion de la UART para la comunicación con el ESP32
 * @return	Nothing
 */
void UART3_Init(void)
{
    /* Inicializa UART3 */
	Chip_SCU_PinMuxSet(2, 3, (SCU_MODE_PULLDOWN | SCU_MODE_FUNC2));					/* P2_3 : USART3_TXD */
	Chip_SCU_PinMuxSet(2, 4, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC2));/* P2_4 : USART3_RXD */
	Board_UART_Init(LPC_USART3);
	Chip_UART_Init(LPC_USART3);
	Chip_UART_SetBaudFDR(LPC_USART3, 115200);
	Chip_UART_ConfigData(LPC_USART3, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS); /* 8 bit data mode, 1 stop bit, Parity Disable*/
	Chip_UART_TXEnable(LPC_USART3);
	NVIC_ClearPendingIRQ(USART3_IRQn);
	NVIC_EnableIRQ(USART3_IRQn);
}

/**
 * @brief	Inicializacion de los sensores
 * @return	Nothing
 */
int SensorsInit(int *mState, uint8_t mGrp[], uint8_t mLbl[], uint8_t mPort[], uint8_t mPin[], int n){
	int s;
	int statusWord = 0;
	for (s = 0; s < n; s++) {
		mState[s] = owInit(mGrp[s], mLbl[s], mPort[s], mPin[s]); 	// Inicializo sensores
		//DEBUGOUT("S%d: %d\n",s,mState[s]);
		if (mState[s]==0) {
			statusWord |= (1 << s);
		} else {
			statusWord |= (0 << s);
		}
	}
	return statusWord;
}

/**
 * @brief	Lectura de la temperatura de los sensores
 * @return	Nothing
 */
void ReadTemp(float32_t * pTempRAW, int mState[], uint8_t mPort[], uint8_t mPin[], int n) {
	int s;
	for (s = 0; s < n; s++) {
		if (mState[s]==0) {
			pTempRAW[s] = owReadTemperature(mPort[s],mPin[s]);
			//showTemperatureI(s, (int) pTempRAW[s]);
		} else {
			pTempRAW[s] = 0.0;
			//showTemperatureI(s, (int) pTempRAW[s]);
		}
	}
}

/**
 * @brief	Separar parte decimal y parte entera del valor de temperatura y convertirlas en enteros
 * @return	Nothing
 */
void SplitTemp(float32_t *pTemp, uint8_t *pE, uint8_t *pD, int n) {
	int s;
	float E, D;
	for (s = 0; s < n; s++) {
		D = modff(pTemp[s], &E);
		pE[s] = (uint8_t) E;
		pD[s] = (uint8_t) (D * 100.0) ;
	}
}


