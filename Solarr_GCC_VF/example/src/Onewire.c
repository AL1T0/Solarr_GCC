/* Copyright 2021, Alena Grebneva
 * Ported from Onewire mbed arm libraries. Adapted to the application
 * to dymnamically select the GPIO pins where sensors are connected
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Brief for this file.
 **
 **/

/** \addtogroup groupName Group Name
 ** @{ */

/*==================[inclusions]=============================================*/

#include "board.h"
#include "arm_math.h"
#include "../inc/Onewire.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
#define TICKRATE 	(1350000)	/* 1350000 ticks per second */

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
/**
 * @brief	Delay en microsegundos generado por el contador de ciclos del DWT
 * @return	Nothing
 */

static void pauseus(uint32_t t)
{
	//uint32_t c = 0;
	DWT->CTRL |= 1;
	DWT->CYCCNT = 0;
	t *= (SystemCoreClock/1000000);
	while((DWT->CYCCNT < t));
	//DWT->CTRL |= ~DWT_CTRL_CYCCNTENA_Msk;
}

/**
 * @brief	Reset y detección de sensor
 * @return	static int
 */
static int owPresence(uint8_t port, uint8_t pin)
{
	unsigned int ret = 1;

	pauseus(1000);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, port, pin); 	//owOUT
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, port, pin); 		//owLOW

	pauseus(480);

	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, port, pin); 	//owIN

	pauseus(40);

	ret = Chip_GPIO_GetPinState(LPC_GPIO_PORT, port, pin);

	pauseus(480);

	return ret;
}

/**
 * @brief	Leer byte desde el sensor
 * @return	unsigned char
 */
static unsigned char owReadByte(/*void * buffer8,*/ uint8_t port, uint8_t pin)
{
    unsigned char i;
    unsigned char data = 0x00;
    for (i = 0; i < 8; i++)
    {
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, port, pin); 	//owOUT
		//pauseus(2);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, port, pin); 		//owLOW
		pauseus(3);

		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, port, pin); 	//owIN
		pauseus(12);

        data >>= 1;	// Hacer espacio para el siguiente bit

        if (Chip_GPIO_GetPinState(LPC_GPIO_PORT, port, pin))
        {
           data |= 0x80;	// Si el pin está en alto, establecer bit en 1 1
        }
        pauseus(55);
    }
    return data;
}

/**
 * @brief	Escribir comandos al sensor
 * @return	Nothing
 */
static void owWriteByte(uint8_t cmd, /*void * buffer, uint8_t n,*/ uint8_t port, uint8_t pin)
{
	unsigned char i = 0;
	for (i = 0; i < 8; i++)
	{
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, port, pin); 	//owOUT
		//pauseus(2);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, port, pin); 		//owLOW
		pauseus(3);

		if (cmd & 0x01)
		{
			Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, port, pin); 	//owLOW
			pauseus(3);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, port, pin); 	//owHIGH
			pauseus(60);
		}
		else
		{
			//Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, port, pin); 	//owLOW
			pauseus(60);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, port, pin); 	//owHIGH
			pauseus(10);
		}
		//pauseus(15);

		cmd >>= 1;

		//pauseus(45);
	}
	//pauseus(10);
	//Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, port, pin); 	//owIN
}

/*==================[external functions definition]==========================*/

/**
 * @brief	Inicialización de los sensores
 * @return	int
 */
int owInit(uint8_t group, uint8_t label, uint8_t port, uint8_t pin)
{
	int s;

	//DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

    /* Init pin */
    Chip_SCU_PinMux(group, label, MD_PUP|MD_EZI, FUNC0);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, port, pin); //owIN

    s = owPresence(port,pin);
    return s;
}

/**
 * @brief	Leer temperatura del sensor
 * @return	Temperatura sin procesar en formato int
 */
float owReadTemperature(uint8_t port, uint8_t pin)
{
    int n;
    float output = 0;
    uint16_t rawDataMerge;
    uint16_t reverseRawDataMerge;
    uint8_t rawData[9] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};

    if(owPresence(port,pin)==0)
  	{
    	pauseus(400);

    	__set_PRIMASK(1);
		owWriteByte(0xCC,port,pin); //command for Jump over ROM for single (only one) DS18B20 working mode
		owWriteByte(0x44,port,pin);	//command for start conversion enviroment Temperature and save in 9Byte RAM in DS18B20
		__set_PRIMASK(0);

		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, port, pin); //owIN();
		while(Chip_GPIO_GetPinState(LPC_GPIO_PORT, port, pin) == false); /* esperar fin de la conversión */

		owPresence(port,pin);
		pauseus(400);

		__set_PRIMASK(1);
		owWriteByte(0xCC,port,pin);	//command for Jump over ROM for single (only one) DS18B20 working mode
		owWriteByte(0xBE,port,pin);	//command for read Temperature Raw Data from DS18B20
		__set_PRIMASK(0);

		/* Leer 9 bytes desde la RAM del DS18B20*/
		for( n = 0; n < 9; n++)
		{

			rawData[n] = owReadByte(port,pin);
		}

		/* Juntar los primeros dos bytes en un entero y convertir a grados celsius */
	    /* Judge Positive(+) or Negative(-) Temperature */
	    if (rawData[1] & 0xF8)
	    {
	        //Negative
	        rawDataMerge = rawData[1];
	        rawDataMerge <<= 8;
	        rawDataMerge += rawData[0];
	        reverseRawDataMerge = ~rawDataMerge;
	        output = -(reverseRawDataMerge + 1);
	    }
	    else
	    {
	        //Positive
	        rawDataMerge = rawData[1];
	        rawDataMerge <<= 8;
	        rawDataMerge += rawData[0];
	        output = rawDataMerge;
	    }

/*		output = rawData[1];
		output <<= 8;
		output |= rawData[0];*/
  	}
    return output;
}

/**
 * @brief	Convertir temperatura leída a formato de punto flotante
 * @return	Temperatura sin procesar en formato float32_t
 */
//float32_t owReadTemp_f32(uint8_t port, uint8_t pin)
//{
//	int rv = owReadTemperature(port, pin);
//	float32_t rawT = rv;
//	return rawT;
//}

/*==================[end of file]============================================*/
